# datasets

- `baseline.hdf5`

Containing power traces collected during the first round of an unprotected implementation of AES128 encryption.

- `masked.hdf5`

Containing power traces collected during AES128 encryption, only masking is used. The trigger reveals the first round.
It contains unsafe dataset (where there is first order leakage caused by how the compiler convert the C++ code to binary), and safe (problem fixed, no more first order leakage).

- `trigger.hdf5`

Containing power traces collected on the protected (but with unsafe masking) AES implementation for Arduino.
Traces collected on two different devices, trigger just before the start of the first round subbytes for the first byte of the key (note that there's small chance that an interrupt occur between the trigger and the target operation, but is unlikely).

- `full.hdf5`

Containing power traces collected on the protected (but with unsafe masking) AES implementation for Arduino. Traces collected on two different devices, no trigger available.


# dataset details

```python
import h5py

def info(fname):
  with h5py.File(fname, "r") as f:
    f.visit(lambda x : print(
      f"+ {x}: {f[x].attrs['info']}" if type(f[x]) == h5py.Group
      else f"  - {x} {f[x].shape} [{f[x].dtype}]: {f[x].attrs['info']}"
    ))
```

```python
DATASET = "simple.hdf5"
info(DATASET)
```
